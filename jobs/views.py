from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse
from django.http.response import HttpResponse, HttpResponseRedirect
import json
from main.decorators import check_mode,ajax_required
from main.functions import generate_form_errors, get_auto_id, get_a_id, get_company_a_id
import datetime
from django.db.models import Q
from jobs.models import JobVacancy, JobInterest, Interview
from jobs.forms import JobVacancyForm
from employers.models import Employer
from candidates.models import Candidate
from main.decorators import role_required
from datetime import datetime
import pytz
from django.conf import settings
from django.template.loader import render_to_string
from users.functions import send_email
from jobs.functions import as_json
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_GET


@check_mode
def post_job(request):    
    employer = get_object_or_404(Employer.objects.filter(user=request.user))
    if request.method == 'POST':
        form = JobVacancyForm(request.POST,request.FILES)
        
        if form.is_valid() : 
            category = form.cleaned_data['job_category']
            if not JobVacancy.objects.filter(employer__user=request.user,job_category=category,is_deleted=False).exists():
                auto_id = get_auto_id(JobVacancy)
                a_id = get_company_a_id(JobVacancy,request)
                #create product
                data = form.save(commit=False)
                data.auto_id = auto_id
                data.a_id = a_id
                data.creator = request.user
                data.updator = request.user
                data.employer = employer
                data.save()
                url = reverse('web:employer_profile') + "?job_list=True"
                return HttpResponseRedirect(url)
            else:
                message = "Job already exists"
                context = {
                    "title" : "Post Job Vacancy",
                    "form" : form,
                    "url" : reverse('jobs:post_job'),
                    "is_need_select" : True,
                    "is_need_datepicker" : True,
                    "employer" : employer,
                    "message" : message,
                }
                return render(request,'web/jobs/post.html',context)
        
        else:
            message = generate_form_errors(form,formset=False)
            context = {
                "title" : "Post Job Vacancy",
                "form" : form,
                "url" : reverse('jobs:post_job'),
                "is_need_select" : True,
                "is_need_datepicker" : True,
                "employer" : employer,
                "message" : message,
            }
            return render(request,'web/jobs/post.html',context)
    
    else:       
        form = JobVacancyForm()
        context = {
            "title" : "Profile",
            "form" : form,
            "url" : reverse('jobs:post_job'),
            "redirect" : True,
            "is_need_select" : True,
            "is_need_datepicker" : True,
            "employer" : employer,
        }
        return render(request,'web/jobs/post.html',context)


@check_mode
def jobs(request):
    instances = JobVacancy.objects.filter(is_deleted=False)
    query = request.GET.get("q")
    title = "Job Vacancies"
    if query:
        instances = instances.filter(Q(auto_id__icontains=query) | Q(employer__banner_name__icontains=query) | Q(employer__film_name__icontains=query) | Q(job_category__icontains=query) | Q(last_date__icontains=query))
        title = "Job Vacancies - %s" %query 
    
    context = {
        "instances" : instances,
        'title' : title,
        
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_wave_effect" : True,
    }
    return render(request,'jobs/jobs.html',context)


@check_mode
@role_required(['company','superadmin'])
def company_jobs(request):
    instances = JobVacancy.objects.filter(is_deleted=False,company__user=request.user)
    query = request.GET.get("q")
    
    context = {
        "instances" : instances,
        'title' : "Companies",

        "is_need_custom_scroll_bar" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_wave_effect" : True,
    }
    return render(request,'web/jobs/careers.html',context) 


@check_mode
def job(request,pk):
    instance = get_object_or_404(JobVacancy.objects.filter(pk=pk,is_deleted=False))
    context = {
        "instance" : instance,
        "title" : instance.name,

        "is_need_custom_scroll_bar" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_wave_effect" : True,
    }
    return render(request,'jobs/job.html',context)


@check_mode
def edit(request,pk):
    instance = get_object_or_404(JobVacancy.objects.filter(pk=pk,is_deleted=False)) 
    employer = get_object_or_404(Employer.objects.filter(user=request.user))
    if request.method == 'POST':
        form = JobVacancyForm(request.POST,request.FILES,instance=instance)
        
        if form.is_valid() : 
            
            data = form.save(commit=False)
            data.updator = request.user
            data.date_updated = datetime.now()
            data.save() 
            
            return HttpResponseRedirect(reverse('web:employer_profile'))  
        
        else:
            message = generate_form_errors(form,formset=False)
            context = {
                "title" : "Post Job Vacancy",
                "form" : form,
                "url" : reverse('jobs:edit' ,kwargs={'pk':instance.pk}),
                "employer" : employer,
                "message" : message,
                "is_need_select" : True,
                "is_need_datepicker" : True,
            }
            return render(request,'web/jobs/post.html',context)
    
    else:       
        form = JobVacancyForm(instance=instance)
        context = {
            "title" : "Profile",
            "form" : form,
            "employer" : employer,
            "url" : reverse('jobs:edit' ,kwargs={'pk':instance.pk}),
            "redirect" : True,
            "is_need_select" : True,
            "is_need_datepicker" : True,
        }
        return render(request,'web/jobs/post.html',context)


@check_mode
@ajax_required
def delete(request,pk):
    instance = get_object_or_404(JobVacancy.objects.filter(pk=pk,is_deleted=False))
    JobVacancy.objects.filter(pk=pk).update(is_deleted=True,is_active=False)
    JobInterest.objects.filter(vacancy=instance).update(is_deleted=True)
    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Job Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('web:employer_profile')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
def delete_selected_jobs(request):
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]
        
        pks = pks.split(',')
        for pk in pks:      
            instance = get_object_or_404(JobVacancy.objects.filter(pk=pk,is_deleted=False)) 
            JobVacancy.objects.filter(pk=pk).update(is_deleted=True,name=instance.name + "_deleted_" + str(instance.auto_id))
            
        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Job(s) Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('jobs:jobs')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some items first.",
        }
        
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@ajax_required
@role_required(['candidate'])
def apply(request,pk):
    candidate = get_object_or_404(Candidate.objects.filter(user=request.user))
    vacancy = get_object_or_404(JobVacancy.objects.filter(is_deleted=False,is_active=True,pk=pk))
    #time conversion from UTC to IST
    if JobInterest.objects.filter(vacancy__pk=pk,candidate=candidate).exists():
        response_data = {
            "status" : "false",
            "title" : "Already Applied",
            "message" : "You already applied for this job.",
        }
    else:
        now = datetime.now()
        current_zone = settings.TIME_ZONE
        pre_time_zone = pytz.timezone(current_zone)
        post_time_zone = pytz.timezone('Asia/Calcutta')
        foreign_time = pre_time_zone.localize(datetime(now.year, now.month, now.day, now.hour, now.minute, now.second))
        local_time = foreign_time.astimezone(post_time_zone)
        #conversion end here
        JobInterest.objects.create(
            auto_id = get_auto_id(JobInterest),
            a_id = get_a_id(JobInterest,request),
            date_added = local_time,
            date_updated = local_time,
            vacancy = vacancy,
            candidate = candidate
        )
        response_data = {
            "status" : "true",
            "title" : "Sucess",
            "message" : "Job Successfully Applied.",
            "redirect" : "true",
            "redirect_url" : reverse('web:candidate_profile'),
        }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
def applied_jobs(request):
    instances = JobInterest.objects.filter(is_deleted=False)
    query = request.GET.get("q")
    title = "Applied Job"
    if query:
        instances = instances.filter(Q(auto_id__icontains=query) | Q(vacancy__employer__banner_name__icontains=query) | Q(candidate__name__icontains=query) | Q(vacancy__employer__film_name__icontains=query) | Q(vacancy__job_category__icontains=query) | Q(vacancy__last_date__icontains=query))
        title = "Applied Job - %s" %query 
    
    context = {
        "instances" : instances,
        'title' : title,

        "is_need_custom_scroll_bar" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_wave_effect" : True,
    }
    return render(request,'jobs/applied.html',context)


@check_mode
@ajax_required
@role_required(['employer'])
def publish(request,pk):
    vacancy = JobVacancy.objects.filter(pk=pk).update(is_active=True)
    response_data = {
        "status" : "true",
        "title" : "Sucess",
        "message" : "Job Successfully published.",
        "redirect" : "true",
        "redirect_url" : reverse('web:employer_profile'),
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')

@check_mode
@role_required(['employer'])
def select(request,pk):
    JobInterest.objects.filter(pk=pk).update(is_selected=True)
    url = reverse('web:employer_profile') + "?interested=True"
    return HttpResponseRedirect(url)

@check_mode
@role_required(['employer'])
def unselect(request,pk):
    JobInterest.objects.filter(pk=pk).update(is_selected=False)
    url = reverse('web:employer_profile') + "?interested=True"
    return HttpResponseRedirect(url)


def get_job_applied_list(request):
    instances = []
    pk = request.GET.get("vacancy")
    if pk :
        vacancy = JobVacancy.objects.get(pk=pk)
        instances = JobInterest.objects.filter(vacancy=vacancy)
    
    results = [as_json(request,ob) for ob in instances]

    mye = {
        "results" : results,
        "film" : vacancy.employer.film_name,
        "category" : vacancy.job_category,
        "last_date" : str(vacancy.last_date),
    }
    return HttpResponse(json.dumps(mye), content_type="application/json")


@check_mode
@ajax_required
@login_required
@require_GET
def select_candidate(request):
    pk = request.GET.get('pk')
    candidate = request.GET.get('candidate')
    selected = 'false'
    if JobInterest.objects.filter(candidate=candidate,pk=pk).exists():
        if JobInterest.objects.filter(candidate=candidate,pk=pk,is_selected=True):
            JobInterest.objects.filter(candidate=candidate,pk=pk).update(is_selected=False)
            selected = 'false'
        else:
            JobInterest.objects.filter(candidate=candidate,pk=pk).update(is_selected=True)
            selected = 'true'
        response_data = {
            "status" : "true",
            "selected" : selected
        }
    else:
        response_data = {
            "status" : "false",
        }
    
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')



def get_job_selected_list(request):
    instances = []
    pk = request.GET.get("vacancy")
    if pk :
        vacancy = JobVacancy.objects.get(pk=pk)
        instances = JobInterest.objects.filter(vacancy=vacancy,is_selected=True,is_interviewed=False)
    
    results = [as_json(request,ob) for ob in instances]

    mye = {
        "results" : results,
        "film" : vacancy.employer.film_name,
        "category" : vacancy.job_category,
        "last_date" : str(vacancy.last_date),
    }
    return HttpResponse(json.dumps(mye), content_type="application/json")


@check_mode
def interview_call(request,pk):    
    employer = get_object_or_404(Employer.objects.filter(user=request.user))
    vacancy = get_object_or_404(JobVacancy.objects.filter(pk=pk))
    if request.method == 'POST':
        place = request.POST.get('place')
        date = request.POST.get('date')
        from_time = request.POST.get('from_time')
        to_time = request.POST.get('to_time')
        contact_number = request.POST.get('contact_number')
        contact_number2 = request.POST.get('contact_number2')
        datetime_obj = datetime.strptime(str(date), '%d/%m/%Y')
        date = datetime_obj.date()
        auto_id = get_auto_id(Interview)
        a_id = get_a_id(Interview,request)
        #create interview details
        interview = Interview.objects.create(
            auto_id = auto_id,
            a_id = a_id,
            place = place,
            date = date,
            from_time = from_time,
            to_time = to_time,
            contact_number = contact_number,
            second_contact_number = contact_number2,
            vacancy =  vacancy 
        )
        JobInterest.objects.filter(is_selected=True,vacancy=vacancy).update(is_interviewed=True,interview=interview)
        # response_data = {
        #     "status" : "true",
        #     "title" : "Successfully Created",
        #     "message" : "Interview Call created sucessfully .",
        #     "redirect" : "true",
        #     "redirect_url" : reverse('web:employer_profile')
        # }

        return HttpResponseRedirect(reverse('web:employer_profile'))
    else:
        # response_data = {
        #     "status" : "false",
        #     "stable" : "true",
        #     "title" : "Form validation error",
        # }
        return HttpResponseRedirect(reverse('web:employer_profile'))
    # return HttpResponse(json.dumps(response_data), content_type='application/javascript')