from django.shortcuts import render, get_object_or_404
from django.http.response import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from users.functions import get_current_role
from employers.models import Employer
from main.decorators import role_required, check_client
from django.contrib.auth.models import User
from candidates.models import Candidate, Education, Experience,Video
from jobs.forms import JobVacancyForm
from django.forms.formsets import formset_factory
from django.forms.models import inlineformset_factory
from jobs.models import JobVacancy, JobInterest
from django.contrib.auth.decorators import login_required
from web.utils import validate_image_size, image_id_from_url
import json
from datetime import datetime
from django.conf import settings
import pytz
from django.db.models import Q
from web.forms import UploadForm, UploadPicForm,CandidateFilterForm
from main.decorators import check_mode,ajax_required
from main.functions import generate_form_errors, get_auto_id, get_a_id
from web.models import PromotionVideo


@check_mode
@check_client
def home(request):
    current_role = get_current_role(request)
    if current_role == 'employer':
        return HttpResponseRedirect(reverse('web:employer_profile'))
    elif current_role == 'candidate':
        return HttpResponseRedirect(reverse('web:candidate_profile'))
    elif current_role == 'superadmin':
        return HttpResponseRedirect(reverse('app'))
    else:
        context = {
            'title' : 'Home',
            'is_home' : True,
        }
 
        return render(request,"web/index.html",context)


@check_mode
@check_client
def about(request):
    context = {
        'title' : 'About',
        'is_about' : True,
    }
    return render(request,"web/about.html",context)


@check_mode
@check_client
def privacy(request):
    context = {
        'title' : 'Privacy Policy',
        'is_privacy' : True,
    }
    return render(request,"web/privacy.html",context)


@check_mode
@check_client
def contact(request):
    context = {
        'title' : 'About',
        'is_contact' : True,
    }
    return render(request,"web/contact.html",context)


@check_mode
@check_client
def services(request):
    context = {
        'title' : 'Services',
        'is_contact' : True,
    }
    return render(request,"web/services.html",context)


@check_mode
@check_client
@login_required
def careers(request):
    current_role = get_current_role(request)
    if current_role == 'employer':
        careers = JobVacancy.objects.filter(is_deleted=False,employer__user=request.user)
    else:
        careers = JobVacancy.objects.filter(is_active=True)
    context = {
        'title' : 'Careers',
        "is_careers": True,
        "careers" : careers
    }
    return render(request,"web/jobs/careers.html",context)


@check_mode
@check_client
@login_required
def employers(request):
    current_role = get_current_role(request)
    instances = Employer.objects.filter(is_active=True,is_deleted=False)
    context = {
        'title' : 'Employers',
        "is_employers": True,
        "careers" : careers,
        "instances" :instances
    }
    return render(request,"web/employer/employers.html",context)


@check_mode
@check_client
@login_required
def candidates(request):
    current_role = get_current_role(request)
    instances = Candidate.objects.filter(is_active=True,is_deleted=False)
    category = request.GET.get('category')
    print category
    if category:
        instances = instances.filter(profile=category)
    context = {
        'title' : 'Candidates',
        "is_employers": True,
        "careers" : careers,
        "instances" :instances,
        "form" : CandidateFilterForm(initial={'category':category}),
        "is_need_page_slider" : True
    }
    return render(request,"web/candidate/candidates.html",context)


@check_mode
@check_client
@login_required
def candidate(request,pk):
    current_role = get_current_role(request)
    instance = get_object_or_404(Candidate.objects.filter(is_deleted=False,pk=pk))
    educations = Education.objects.filter(candidate=instance)
    experiences = Experience.objects.filter(candidate=instance)
    videos = Video.objects.filter(candidate=instance)
    context = {
        'title' : 'Employers',
        "is_employers": True,
        "careers" : careers,
        "instance" :instance,
        "educations" : educations,
        "experiences" : experiences,
        "videos" : videos,
        "is_need_video_slider" : True,
    }
    return render(request,"web/candidate/candidate.html",context)


@check_mode
@check_client
@role_required(['employer'])
@login_required
def employer_profile(request):
    instance = get_object_or_404(Employer.objects.filter(is_deleted=False,user=request.user))
    job_lists = JobVacancy.objects.filter(is_deleted=False,employer__user=request.user)
    published_lists = job_lists.filter(is_deleted=False,employer__user=request.user)
    interested_lists = JobInterest.objects.filter(is_deleted=False,vacancy__employer__user=request.user)
    job_form = JobVacancyForm()
    is_profile = True
    is_interested = False
    is_job_list = False
    interest = request.GET.get('interested')
    job_list = request.GET.get('job_list')
    if interest == "True":
        is_profile = False
        is_interested = True
    if job_list == "True":
        is_profile = False
        is_job_list = True

    context = {
        'title' : 'Profile',
        'instance' : instance,
        'job_form' : job_form,
        "job_lists" : job_lists,
        "published_lists" : published_lists,
        "interested_lists" : interested_lists,
        'is_profile' : is_profile,
        "is_interested" : is_interested,
        "is_job_list" : is_job_list,
        "is_need_select" : True,
        "is_need_datepicker" : True,
        'url' : reverse('jobs:post_job'),
        "profile" : True,
        "is_need_crop" : True,
        "is_need_date_time_picker" : True,
    }
    return render(request,"web/employer/employer-profile.html",context)


@check_mode
@check_client
@role_required(['candidate'])
@login_required
def candidate_profile(request):
    instance = get_object_or_404(Candidate.objects.filter(is_deleted=False,user=request.user))
    educations = Education.objects.filter(candidate=instance)
    experiences = Experience.objects.filter(candidate=instance)
    interest_list = JobInterest.objects.filter(is_deleted=False,candidate__user=request.user).values('vacancy')
    job_lists = JobVacancy.objects.filter(is_active=True,job_category=instance.profile,state=instance.state).exclude(pk__in=interest_list)
    jobs = JobInterest.objects.filter(is_deleted=False,candidate__user=request.user).order_by('-date_added')
    applied_jobs = jobs
    selected_jobs = jobs.filter(is_selected=True,is_interviewed=True,is_completed=False)
    videos = Video.objects.filter(candidate=instance)
    context = {
        'title' : 'Profile',
        'instance' : instance,
        'is_profile' : True,
        'applied_jobs' : applied_jobs,
        "educations" : educations,
        "experiences" : experiences,
        "job_lists" : job_lists,
        "selected_jobs" : selected_jobs,
        "videos" : videos,
    }
    return render(request,"web/candidate/candidate-profile.html",context)


@check_mode
@check_client
@role_required(['student'])
@login_required
def student_profile(request):
    student = get_object_or_404(Student.objects.filter(is_deleted=False,user=request.user))
    context = {
        'title' : 'Profile',
        'student' : student,
        'is_student_profile' : True,
    }
    return render(request,"web/students/student-profile.html",context)


@check_mode
@check_client
@login_required
def upload_cover(request):
    pk = request.GET.get('pk')
    current_role = get_current_role(request)
    if Employer.objects.filter(pk=pk).exists():
        instance = Employer.objects.get(pk=pk)
        if request.method == 'POST':
            form = UploadForm(request.POST,request.FILES,instance=instance)
            if form.is_valid():
                cover = form.save(commit=False)
                cover.save()
            if current_role == "employer":
                return HttpResponseRedirect(reverse('web:employer_profile'))
            else: 
                return HttpResponseRedirect(reverse('web:candidate_profile'))


@check_mode
@check_client
@login_required
def upload_candidate_cover(request):
    pk = request.GET.get('pk')
    current_role = get_current_role(request)
    if Candidate.objects.filter(pk=pk).exists():
        print "ok"
        instance = Candidate.objects.get(pk=pk)
        if request.method == 'POST':
            form = UploadPicForm(request.POST,request.FILES,instance=instance)
            if form.is_valid():
                cover = form.save(commit=False)
                cover.save()
    return HttpResponseRedirect(reverse('web:candidate_profile'))



@check_mode
@ajax_required
@role_required(['candidate'])
@login_required
def promote_video(request,pk):
    instance = get_object_or_404(Video.objects.filter(pk=pk))
    if PromotionVideo.objects.filter(video=instance,is_active=True).exists():
        response_data = {
            "status" : "false",
            "title" : "Already Promoted",
            "message" : "This video already in promotion list.",
        }
    else:
        now = datetime.now()
        current_zone = settings.TIME_ZONE
        pre_time_zone = pytz.timezone(current_zone)
        post_time_zone = pytz.timezone('Asia/Calcutta')
        foreign_time = pre_time_zone.localize(datetime(now.year, now.month, now.day, now.hour, now.minute, now.second))
        local_time = foreign_time.astimezone(post_time_zone)
        promoted_date = local_time.date()
        #creating promotoin instance
        auto_id = get_auto_id(PromotionVideo)
        a_id = get_a_id(PromotionVideo,request)
        promoted_video = PromotionVideo.objects.create(
            video=instance,
            auto_id=auto_id,
            a_id = a_id,
            promoted_date = promoted_date,
            end_date = promoted_date,
            creator= request.user,
            updator = request.user,
        )
        response_data = {
            "status" : "true",
            "title" : "Promtion Sucessfull",
            "message" : "Please click ok to complete payment.",
            "redirect" : "true",
            "redirect_url" : reverse('instamojo:payment')+"?promotion=True"+"&video="+str(promoted_video.pk),
        }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
def promoted_videos(request):
    instances = PromotionVideo.objects.filter(is_deleted=False,is_active=True)
    query = request.GET.get("q")
    title = "Promoted Videos"
    if query:
        instances = instances.filter(Q(auto_id__icontains=query) | Q(video__candidate__name__icontains=query))
        title = "Promoted Videos - %s" %query 
    
    
    context = {
        "instances" : instances,
        'title' : title,

        "is_need_custom_scroll_bar" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_wave_effect" : True,
        "is_need_popup_box" : True,
    }
    return render(request,'web/promoted_videos.html',context)


@check_mode
def promoted_video(request,pk):
    instance = get_object_or_404(PromotionVideo.objects.filter(pk=pk,is_deleted=False))
    context = {
        "instance" : instance,
        "title" : "Promotion Video",

        "is_need_custom_scroll_bar" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_wave_effect" : True,
        "is_need_popup_box" : True,
    }
    return render(request,'web/promoted_video.html',context)



def handler500(request):
    
    context = {
        'title' : "Error 500",
        "body_class":"server_error",
        "short_description" : "We're sorry! The server encountered an internal error",
    }
    template = "errors/500.html"
    response = render(request,template,context)
    
    response.status_code = 500
    return response


def handler404(request):
    
    context = {
        'title' : "Error 404",
        "body_class":"server_error",
        "short_description" : "It seems we can't find what you're looking for",
    }
    template = "errors/404.html"
    response = render(request,template,context)
    
    response.status_code = 404
    return response


def handler403(request):
    
    context = {
        'title' : "Error 403",
        "body_class":"server_error",
        "short_description" : "You're not authorized to view this page.",
    }
    template = "errors/403.html"
    response = render(request,template,context)
    
    response.status_code = 403
    return response


def handler400(request):
    
    context = {
        'title' : "Error 400",
        "body_class":"server_error",
        "short_description" : "Your browser sent a request that this server could not understand",
    }
    template = "errors/400.html"
    response = render(request,template,context)
    
    response.status_code = 400
    return response 


@check_mode
@ajax_required
def delete(request,pk):
    instance = get_object_or_404(PromotionVideo.objects.filter(pk=pk,is_deleted=False))
    PromotionVideo.objects.filter(pk=pk).update(is_deleted=True)

    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Promotion Video Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('web:promoted_videos')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')