from django.core.management.base import BaseCommand, CommandError
from companies.models import Company
from users.functions import send_email
import random, string
from django.template.loader import render_to_string

class Command(BaseCommand):

    def handle(self, *args, **options):
        instances = PromotionVideo.objects.filter(is_deleted=False,is_active=True)
        for instance in instances:
            last_date = instance.end_date
            if last_date:
                if date.today() > last_date:
                    instance.is_active = False
                    instance.save()
        