from __future__ import unicode_literals
from django.db import models
from django.utils.translation import ugettext_lazy as _
from versatileimagefield.fields import VersatileImageField
from django.core.validators import MinValueValidator
from decimal import Decimal
from versatileimagefield.fields import VersatileImageField
from main.models import BaseModel
from candidates.constants import GENDER, PROFILE
from jobs.models import JobInterest


class Candidate(BaseModel):
    user = models.OneToOneField("auth.user",blank=True,null=True)
    #registration
    profile = models.CharField(max_length=128,choices=PROFILE)
    name = models.CharField(max_length=128)
    dob = models.DateField()
    gender = models.CharField(max_length=128,choices=GENDER)
    mother_tounge = models.CharField(max_length=128)
    mobile_number = models.CharField(max_length=128)
    email = models.EmailField()
    place = models.CharField(max_length=128)
    pin = models.CharField(max_length=128,blank=True,null=True)
    state = models.ForeignKey('candidates.State')
    #profile
    height = models.CharField(max_length=128,blank=True,null=True)
    weight = models.CharField(max_length=128,blank=True,null=True)
    body_type = models.CharField(max_length=128,blank=True,null=True)
    skin_tone = models.CharField(max_length=128,blank=True,null=True)
    address = models.TextField(blank=True,null=True)
    profile_photo = VersatileImageField('Photo',upload_to="candidates/photo/",blank=True,null=True)
    natural_photo = VersatileImageField('Photo',upload_to="candidates/photo/",blank=True,null=True)
    cover_photo = VersatileImageField('Photo',upload_to="candidates/photo/",blank=True,null=True)
    edited_photo = VersatileImageField('Photo',upload_to="candidates/photo/",blank=True,null=True)
    makeup_photo = VersatileImageField('Photo',upload_to="candidates/photo/",blank=True,null=True)
    side_photo = VersatileImageField('Photo',upload_to="candidates/photo/",blank=True,null=True)
    additional_info = models.TextField(blank=True,null=True)
    # Education
    work_attended = models.CharField(max_length=128,blank=True,null=True)
    media_related_studies = models.CharField(max_length=128,blank=True,null=True)
    #about
    about = models.TextField(blank=True,null=True)
    dreams = models.TextField(blank=True,null=True)
    media_experience = models.TextField(blank=True,null=True)
    passion_extra_skills = models.TextField(blank=True,null=True)

    is_deleted = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    is_reg_fee_payed = models.BooleanField(default=False)
    class Meta:
        db_table = 'candidates_candidate'
        verbose_name = _('candidate')
        verbose_name_plural = _('candidates')
        ordering = ('name',)
  
    def __unicode__(self): 
        return self.name +" " +str(self.mobile_number)

    def educations(self):
        return Education.objects.filter(candidate=self)

    def experiences(self):
        return Experience.objects.filter(candidate=self)

    def videos(self):
        return Video.objects.filter(candidate=self)

    def is_apply(self):
        if JobInterest.objects.filter(candidate=self).exists():
            return True
        else:
            return False


class Education(models.Model):
    candidate = models.ForeignKey('candidates.Candidate')
    name = models.CharField(max_length=128)
    class Meta:
        db_table = 'candidates_educations'
        verbose_name = _('education')
        verbose_name_plural = _('educations')
        ordering = ('name',)
  
    def __unicode__(self): 
        return self.name


class Experience(models.Model):
    candidate = models.ForeignKey('candidates.Candidate')
    experience = models.CharField(max_length=128)
    class Meta:
        db_table = 'candidates_experiences'
        verbose_name = _('experience')
        verbose_name_plural = _('experiences')
        ordering = ('experience',)
  
    def __unicode__(self): 
        return self.experience


class Video(models.Model):
    candidate = models.ForeignKey('candidates.Candidate')
    video_url = models.CharField(max_length=128)
    video_code = models.CharField(max_length=128,blank=True,null=True)
    class Meta:
        db_table = 'candidates_videos'
        verbose_name = _('video')
        verbose_name_plural = _('videos')
        ordering = ('id',)
  
    def __unicode__(self): 
        return self.video_url


class State(models.Model):
    name = models.CharField(max_length=128)
    code = models.CharField(max_length=128)

    class Meta:
        db_table = 'state'
        verbose_name = _('state')
        verbose_name_plural = _('states')
        ordering = ('id',)

    def __unicode__(self):
        return str(self.name)