from django.http import HttpResponse, HttpResponseRedirect
from instamojo.config import API_KEY, AUTH_TOKEN, API_KEY_TEST, AUTH_TOKEN_TEST
from django.urls import reverse
from instamojo.forms import PayForm
from instamojo_wrapper import Instamojo
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.contrib import messages
import logging, traceback
import hashlib
from django.views.decorators.csrf import csrf_exempt
from finance.models import BankAccount
from users.functions import get_current_role
from candidates.models import Candidate
from employers.models import Employer
from main.functions import get_auto_id, get_a_id
from instamojo.models import Payment
from main.decorators import check_mode
import datetime
from django.shortcuts import render, get_object_or_404,redirect
from web.models import PromotionVideo
from django.contrib.auth.models import User
import instamojo.constants as constants


api = Instamojo(api_key=API_KEY, auth_token=AUTH_TOKEN)
api_test = Instamojo(api_key=API_KEY_TEST, auth_token=AUTH_TOKEN_TEST, endpoint='https://test.instamojo.com/api/1.1/')
def payment(request):
    current_role = get_current_role(request)
    registration = request.GET.get('registration')
    promotion = request.GET.get('promotion')
    video = request.GET.get('video')
    is_registration = False
    is_promotion = False
    amount = 0
    if registration:
        if registration == "True":
            is_registration = True
            amount = constants.REGISTRATION_FEE
            purpose = 'Membership'
    if promotion:
        if promotion == "True":
            is_promotion = True
            amount = constants.VIDEO_PROMOTION_FEE
            purpose = 'Promotion'

    if current_role == 'candidate':
        customer = Candidate.objects.get(user=request.user)
        name = customer.name
        phone = customer.mobile_number
        address = customer.address
    else:
        customer = Employer.objects.get(user=request.user)
        name = customer.banner_name
        phone = ''
        address = customer.director_name+" "+customer.banner_name

    email = request.user.email

    if request.method == 'POST':
        form = PayForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            amount = form.cleaned_data['amount']
            email = form.cleaned_data['email']
            name = form.cleaned_data['name']
            phone = form.cleaned_data['phone']
            purpose = form.cleaned_data['purpose']

            currency = 'INR'
            language = 'EN'
            auto_id = get_auto_id(Payment)
            a_id = get_a_id(Payment,request)
            #create pending payment
            payment = Payment.objects.create(
                creator=request.user,
                updator=request.user,
                amount=amount,
                auto_id=auto_id,
                a_id = a_id,
                order_status="Pending/Rejected",
                currency=currency,
                language=language,
                payment_mode="instamojo",
                is_registration = is_registration,
                is_promotion = is_promotion
            )   
            response = api.payment_request_create(
                amount=amount,
                purpose=purpose,
                send_email=False,
                email=email,
                buyer_name=name,
                phone=phone,
                redirect_url=request.build_absolute_uri(reverse("instamojo:payment_success")+"?payment="+str(payment.pk)+"&video="+str(video))
            )
            return HttpResponseRedirect(response['payment_request']['longurl'])

    else:
        form = PayForm(initial={'name':name,'email':email,'phone': phone,'amount':amount,'purpose':purpose})
        context = {
            'form' : form,
            'url' : reverse('instamojo:payment')+"?registration="+str(registration)+"&promotion="+str(promotion)+"?video="+str(video)
        }

    return render(request, 'payment/payment.html', context)


@csrf_exempt
def payment_success(request):
    payment_id = request.GET.get('payment_id')
    payment_request_id = request.GET.get('payment_request_id')
    response = api.payment_request_payment_status(payment_request_id, payment_id)
    current_role = get_current_role(request)
    pk = request.GET.get('payment')

    success = response['success']
    name = response['payment_request']['buyer_name']
    email = response['payment_request']['email']
    phone = response['payment_request']['phone']
    mode = response['payment_request']['payment']['instrument_type']
    transaction_id = response['payment_request']['id']
    status = response['payment_request']['status']
    purpose = response['payment_request']['purpose']

    if success:
        payment_obj = None
        if Payment.objects.filter(pk=pk).exists():
            payment_obj = Payment.objects.get(pk=pk)
        if payment_obj:
            #checking the payment is for registration
            if payment_obj.is_registration:
            
                if current_role == 'candidate':
                    candidate = Candidate.objects.get(user=request.user)
                    candidate.is_reg_fee_payed = True
                    candidate.is_active = True
                    candidate.save()
                else:
                    employer = Employer.objects.get(user=request.user)
                    employer.is_reg_fee_payed = True
                    employer.is_active = True
                    employer.save()
            #checking th epayment is for promotion
            if payment_obj.is_promotion:
                end_date = datetime.date.today() + datetime.timedelta(30)
                video  = request.GET.get('video')
                if video:
                    instance = get_object_or_404(PromotionVideo.objects.filter(pk=video))
                    instance.is_active = True
                    instance.end_date = end_date
                    instance.save()
            #payment checking end here
            payment_obj.billing_name = name
            payment_obj.billing_email = email
            payment_obj.billing_email = purpose
            payment_obj.billing_tel = phone
            payment_obj.order_status = status

            payment_obj.transaction_id = transaction_id
            payment_obj.payment_id = payment_id

            payment_obj.is_completed = True

            payment_obj.save()

            receiver_email = "revolutioncasting123@gmail.com"
            if BankAccount.objects.filter(is_deleted=False,payumoney_email=receiver_email).exists():
                bank_account = BankAccount.objects.get(is_deleted=False,payumoney_email=receiver_email)
                balance = bank_account.balance
                bank_account.balance = balance + Decimal(amount)
                bank_account.save()
                payment_obj.bank_account = bank_account
                payment_obj.save()
   
        return HttpResponseRedirect(reverse('web:home'))
    else:
        if request.user.is_authenticated:
            if Candidate.objects.filter(user=request.user,is_active=False,is_reg_fee_payed=False).exists():
                candidate = Candidate.objects.get(user=request.user,is_active=False,is_reg_fee_payed=False)
                Candidate.objects.filter(user=request.user,is_active=False,is_reg_fee_payed=False).delete()
                User.objects.filter(username=candidate.user.username,email=candidate.user.email).delete()
            elif Employer.objects.filter(user=request.user,is_active=False,is_reg_fee_payed=False).exists(): 
                employer = Employer.objects.get(user=request.user,is_active=False,is_reg_fee_payed=False)       
                Employer.objects.filter(user=request.user,is_active=False,is_reg_fee_payed=False).delete()        
                User.objects.filter(username=employer.user.username,email=employer.user.email).delete()

        data = {
            "true_user" : False,
        }

        return render(request, "payment/failure.html", data)

@csrf_exempt
def payment_failure(request):
    order = request.GET.get('order')
    pk = request.GET.get('order')
    order = None
    if pk:
        if EcommerceOrder.objects.filter(pk=pk).exists():
            order = EcommerceOrder.objects.get(pk=pk)

    if order:
        order.payment_status = "cancelled"
        order.status = "cancelled"
        order.save()

        if order.payment:
            order.payment.order_status = "Cancelled"
            order.payment.save()   

    if Candidate.objects.filter(user=request.user,is_active=True,is_reg_fee_payed=True).exists():
        true_user = True
    elif Candidate.objects.filter(user=request.user,is_active=False,is_reg_fee_payed=False).exists():
        true_user = False
        candidate = Candidate.objects.get(user=request.user,is_active=False,is_reg_fee_payed=False)
        Candidate.objects.filter(user=request.user,is_active=False,is_reg_fee_payed=False).delete()
        User.objects.filter(username=candidate.user.username,email=candidate.user.email).delete()
    elif Employer.objects.filter(user=request.user,is_active=True,is_reg_fee_payed=True).exists():        
        true_user = True
    elif Employer.objects.filter(user=request.user,is_active=False,is_reg_fee_payed=False).exists():
        true_user = False
        employer = Employer.objects.get(user=request.user,is_active=False,is_reg_fee_payed=False)
        Employer.objects.filter(user=request.user,is_active=False,is_reg_fee_payed=False).delete()
        User.objects.filter(username=candidate.user.username,email=candidate.user.email).delete()
    else :
        true_user = False
    data = {
        "true_user" : true_user,
    }

    return render(request, "payment/failure.html", data)

    
@csrf_exempt
def payment_cancel(request):  
    if request.user.is_authenticated:
        if Candidate.objects.filter(user=request.user,is_active=False,is_reg_fee_payed=False).exists():
            candidate = Candidate.objects.get(user=request.user,is_active=False,is_reg_fee_payed=False)
            Candidate.objects.filter(user=request.user,is_active=False,is_reg_fee_payed=False).delete()
            User.objects.filter(username=candidate.user.username,email=candidate.user.email).delete()
        elif Employer.objects.filter(user=request.user,is_active=False,is_reg_fee_payed=False).exists(): 
            employer = Employer.objects.get(user=request.user,is_active=False,is_reg_fee_payed=False)       
            Employer.objects.filter(user=request.user,is_active=False,is_reg_fee_payed=False).delete()        
            User.objects.filter(username=employer.user.username,email=employer.user.email).delete()

    data = {
        "true_user" : False,
    }

    return render(request, "payment/failure.html", data)