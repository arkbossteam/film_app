from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^pay$', views.payment, name='payment'),
    url(r'^payment/success/$', views.payment_success, name="payment_success"),
    url(r'^payment/failure/$', views.payment_failure, name="payment_failure"),
    url(r'^payment/cancel/$', views.payment_cancel, name="payment_cancel"),
]
