from django import forms
from finance.models import Transaction, BankAccount,\
    TransactionCategory
from django.forms.widgets import Select, TextInput, Textarea, CheckboxInput
from django.forms.fields import Field
from dal import autocomplete
setattr(Field, 'is_checkbox', lambda self: isinstance(self.widget, forms.CheckboxInput))
from django.utils.translation import ugettext_lazy as _


TRANSACTION_CATEGORY_TYPES = (
    ('','---------------'),
    ('income','Income'),
    ('expense','Expense'),
)


class BankAccountForm(forms.ModelForm):
    
    class Meta:
        model = BankAccount
        exclude = ['updator','creator','is_deleted','auto_id','balance']
        widgets = {
            'name': TextInput(attrs={'class': 'required form-control','placeholder' : 'Name'}),
            'ifsc': TextInput(attrs={'class': 'required form-control','placeholder' : 'IFSC'}),
            'branch': TextInput(attrs={'class': 'required form-control','placeholder' : 'Branch'}),
            'account_type': Select(attrs={'class': 'required form-control selectpicker'}),
            'account_no': TextInput(attrs={'class': 'required form-control','placeholder' : 'Account number'}),
            'paypal_email': TextInput(attrs={'class': 'required form-control','placeholder' : 'Paypal email'}),
            "first_time_balance" : TextInput(attrs={"class":"required form-control number","placeholder":"Enter amount"}),
        }
        error_messages = {
            'name' : {
                'required' : _("Name field is required."),
            },
            'ifsc' : {
                'required' : _("Ifsc field is required."),
            },
            'branch' : {
                'required' : _("Branch field is required."),
            },
            'account_type' : {
                'required' : _("Account Type field is required."),
            },
            'account_no' : {
                'required' : _("Account No field is required."),
            },
            'paypal_email' : {
                'required' : _("Paypal Email field is required."),
            }
        }
        
    

class TransactionCategoryForm(forms.ModelForm):
    
    class Meta:
        model = TransactionCategory
        exclude = ['updator','creator','is_system_generated','is_deleted','auto_id']
        widgets = {
            'name': TextInput(attrs={'class': 'required form-control','placeholder' : 'Name'}),
            'category_type' : Select(attrs={'class': 'required form-control selectpicker'}),
        }
        error_messages = {
            'name' : {
                'required' : _("Name field is required."),
            },
            'category_type' : {
                'required' : _("Category Type field is required."),
            }
        }
        
        
class TransactionForm(forms.ModelForm): 
    
    class Meta:
        model = Transaction
        exclude = ['creator','updator','transaction_type','auto_id',]
        widgets = {
            "transaction_category" : Select(attrs={"class":"single required form-control selectpicker"}),
            "transaction_mode" : Select(attrs={"class":"single required form-control selectpicker"}),
            "payment_mode" : Select(attrs={"class":"single bank_item required form-control selectpicker"}),
            "bank_account" : Select(attrs={"class":"single bank_item required form-control selectpicker"}),
            "amount" : TextInput(attrs={"class":"required form-control number","placeholder":"Enter amount"}),
            "cheque_details" : Textarea(attrs={"class":"bank_item cheque_item required form-control","rows":"5","placeholder":"Enter cheque details"}),
            "card_details" : Textarea(attrs={"class":"bank_item card_item required form-control","rows":"5","placeholder":"Enter card details"}),
            "is_cheque_withdrawed" : CheckboxInput(attrs={"class":"bank_item cheque_item form-control"}),
            "date" : TextInput(attrs={"class":"date-picker required form-control","placeholder":"Enter date"}),
        }
        error_messages = {
            'transaction_category' : {
                'required' : _("Transaction Category field is required."),
            },
            'transaction_mode' : {
                'required' : _("Transaction Mode field is required."),
            },
            'payment_mode' : {
                'required' : _("Payment Mode field is required."),
            },
            'bank_account' : {
                'required' : _("Bank Account field is required."),
            },
            'amount' : {
                'required' : _("Amount field is required."),
            },
            'cheque_details' : {
                'required' : _("Cheque Details field is required."),
            },
            'card_details' : {
                'required' : _("Card Details field is required."),
            },
            'date' : {
                'required' : _("Date field is required."),
            }
        }