from django.contrib import admin
from main.models import App


class AppAdmin(admin.ModelAdmin):
    list_display = ('date_added','application')
admin.site.register(App,AppAdmin)