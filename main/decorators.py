from django.shortcuts import render
from main.models import Mode
from django.http.response import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
import json
from users.functions import get_current_role
from employers.models import Employer
from candidates.models import Candidate


def ajax_required(function):
    def wrap(request, *args, **kwargs):
        if not request.is_ajax():
            return render(request,'errors/400.html',{})
        return function(request, *args, **kwargs)
    wrap.__doc__=function.__doc__
    wrap.__name__=function.__name__
    return wrap


def check_mode(function):
    def wrap(request, *args, **kwargs):
        mode = Mode.objects.get(id=1)
        readonly = mode.readonly
        maintenance = mode.maintenance
        down = mode.down

        if down:
            if request.is_ajax():
                response_data = {}
                response_data['status'] = 'false'
                response_data['message'] = "Application currently down. Please try again later."
                response_data['static_message'] = "true"
                return HttpResponse(json.dumps(response_data), content_type='application/javascript')
            else:
                return HttpResponseRedirect(reverse('down'))
        elif readonly:
            if request.is_ajax():
                response_data = {}
                response_data['status'] = 'false'
                response_data['message'] = "Application now readonly mode. please try again later."
                response_data['static_message'] = "true"
                return HttpResponse(json.dumps(response_data), content_type='application/javascript')
            else:
                return HttpResponseRedirect(reverse('read_only'))

        return function(request, *args, **kwargs)

    wrap.__doc__=function.__doc__
    wrap.__name__=function.__name__
    return wrap


def permissions_required(permissions,roles=['administrator','app_owner','customer','candidate','company','student'],all_permissions=False,all_roles=False,both_check=False,super_user_ok=True,allow_self=False):

    def _method_wrapper(view_method):

        def _arguments_wrapper(request, *args, **kwargs) :
            current_role = get_current_role(request)
            
            has_permission = False
            permission_ok = False
            role_ok = False


            super_users = User.objects.filter(is_superuser=True,is_active=True)
            
            if permissions:
                booleans = [] 
                if booleans:     
                    if all_permissions:
                        if 0 in booleans:
                            permission_ok = False
                        else:
                            permission_ok = True
                    else:
                        if 1 in booleans:
                            permission_ok = True
            else:
                permission_ok = True    

            if both_check:
                if permission_ok and role_ok:
                    has_permission = True
                else:
                    has_permission = False
            else:
                if permission_ok or role_ok:
                    print permission_ok
                    has_permission = True
                else:
                    has_permission = False
               
            if super_user_ok: 
                if request.user in super_users:
                        has_permission = True
                            
            if allow_self:
                pk = kwargs["pk"] 
                instance = model.objects.get(pk=pk)
                if instance.creator == request.user:
                    has_permission = True  
                        
            if not has_permission:
                if request.is_ajax():
                    response_data = {}
                    response_data['status'] = 'false'
                    response_data['stable'] = 'true'
                    response_data['title'] = 'Permission Denied'
                    response_data['message'] = "You have no permission to do this action."
                    response_data['static_message'] = "true"
                    return HttpResponse(json.dumps(response_data), content_type='application/javascript')
                else:
                    
                    if current_role == "company":
                        return HttpResponseRedirect(reverse('web:index'))
                    else:
                        print current_role
                        context = {
                            "is_need_custom_scroll_bar" : True,
                            "is_need_wave_effect" : True,
                            "is_need_bootstrap_growl" : True,
                            "is_need_animations" : True,

                            "is_need_grid_system" : True,
                            "title" : "Permission Denied"
                        }
                        return render(request, 'errors/permission_denied.html', context)
                
            return view_method(request, *args, **kwargs)

        return _arguments_wrapper

    return _method_wrapper


def role_required(roles):
    def _method_wrapper(view_method):
        def _arguments_wrapper(request, *args, **kwargs) :
            current_role = get_current_role(request)
            if not current_role in roles:
                if request.is_ajax():
                    response_data = {}
                    response_data['status'] = 'false'
                    response_data['stable'] = 'true'
                    response_data['title'] = 'Permission Denied'
                    response_data['message'] = "You have no permission to do this actionx."
                    return HttpResponse(json.dumps(response_data), content_type='application/javascript')
                else:
                    if current_role == "employer" :
                        return HttpResponseRedirect(reverse('web:employer_profile'))

                    if current_role == "candidate" :
                        return HttpResponseRedirect(reverse('web:candidate_profile'))
                    else:
                        context = {
                            "is_need_custom_scroll_bar" : True,
                            "is_need_wave_effect" : True,
                            "is_need_bootstrap_growl" : True,
                            "is_need_animations" : True,

                            "is_need_grid_system" : True,
                            "title" : "Permission Denied"
                        }
                        return render(request, 'web/errors/permission_denied.html', context)
                
            return view_method(request, *args, **kwargs)

        return _arguments_wrapper

    return _method_wrapper
            

def check_client(function):
    def wrap(request, *args, **kwargs):
        current_role = get_current_role(request)

        if current_role == "candidate" :
            if not Candidate.objects.get(user=request.user).is_reg_fee_payed:
                return HttpResponseRedirect(reverse('instamojo:payment')+"?registration=True")

        elif current_role == "employer" :
            if not Employer.objects.get(user=request.user).is_reg_fee_payed:
                return HttpResponseRedirect(reverse('instamojo:payment')+"?registration=True")

        return function(request, *args, **kwargs)

    wrap.__doc__=function.__doc__
    wrap.__name__=function.__name__
    return wrap