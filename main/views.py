from django.shortcuts import render
from django.http.response import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from main.decorators import check_mode, ajax_required
from main.functions import get_auto_id, generate_form_errors, get_a_id
import json
from django.views.decorators.http import require_GET
from users.models import NotificationSubject, Notification
from django.db.models import Sum
from django.contrib.auth.models import Group
import datetime
from calendar import monthrange
from django.utils import timezone
from decimal import Decimal
from main.decorators import role_required
from users.functions import get_current_role
from candidates.models import Candidate
from employers.models import Employer
from jobs.models import JobVacancy
from main.models import App


@login_required
@role_required(['app_owner','superadmin','administrator'])
def app(request):
    return HttpResponseRedirect(reverse('dashboard'))


@login_required
@role_required(['app_owner','superadmin','administrator'])
def dashboard(request):  
    recent_employers = Employer.objects.filter(is_deleted=False,is_active=True).order_by('-date_added')[:10]
    recent_candidates = Candidate.objects.filter(is_deleted=False,is_active=True).order_by('-date_added')[:10]
    recent_vacancies = JobVacancy.objects.filter(is_deleted=False).order_by('-date_added')[:10]
    context = {
        "title" : "Dashboard",
        "recent_employers" : recent_employers,
        "recent_candidates" : recent_candidates,
        "recent_vacancies" : recent_vacancies,


        "is_dashboard" : True,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
        "is_dashboard" :True

    }
    return render(request,"base.html",context)


def verify(request):
    if request.user.is_authenticated:

        current_role = get_current_role(request)

        if current_role == "company":
            return HttpResponseRedirect(reverse('web:company_profile'))
        else:
            return HttpResponseRedirect(reverse('dashboard'))
    else:
        return HttpResponseRedirect(reverse('web:home'))


def download_app(request): 
    app_list = App.objects.all()
    app = None
    file = None
    if app_list:
        app = app_list.latest('date_added')
        file = app.application.url
        if request.is_ajax():
            protocol = "http://"
            if request.is_secure():
                protocol = "https://"

            web_host = request.get_host()
            file_url = protocol + web_host + file

            response_data = {
                "status" : "true",
                "file_url" : file_url
            }
            
            return HttpResponse(json.dumps(response_data), content_type='application/javascript')
        else:
            return HttpResponseRedirect(file)
    else:
        return HttpResponse("File not uploaded.")