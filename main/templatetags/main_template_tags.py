from django.template import Library
from django.contrib.auth.models import User
register = Library()
from django.template.defaultfilters import stringfilter
from jobs.models import JobInterest


@register.filter    
def check_default(value): 
	result = value
	if value == "default":
		result = "-"
	return result
                                                        
    
@register.filter
@stringfilter
def underscore_smallletter(value):
    value =  value.replace(" ", "_")
    return value
    

@register.filter
def to_fixed_two(value):
    return "{:10.2f}".format(value)


@register.filter
def tax_devide(value):
    return value/2


@register.filter
@stringfilter
def upfirstletter(value):
    first = value[0] if len(value) > 0 else ''
    remaining = value[1:] if len(value) > 1 else ''
    return first.upper() + remaining    

@register.filter
def camelize(value):
    if not value == None:
        words = value.split(" ")
        return ' '.join([word.capitalize() for word in words])
    
@register.filter
def is_apply(value,arg):
    if JobInterest.objects.filter(vacancy=value,candidate=arg).exists():
        return True
    else:
        return False



